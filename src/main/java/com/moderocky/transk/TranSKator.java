package com.moderocky.transk;

import ch.njol.skript.Skript;
import ch.njol.skript.SkriptAddon;
import com.moderocky.Bedrock;
import com.moderocky.transk.api.BDI;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

@SuppressWarnings("unused")
public class TranSKator extends JavaPlugin {

    private static TranSKator instance;
    private static BDI blindDataInterface;
    private static String version;

    private final Metrics metrics = new Metrics(this, 6785);

    public static TranSKator getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        version = (Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3]).replace("v", "");

        init();
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    private void init() {
        if ((Bukkit.getPluginManager().getPlugin("Skript") != null) && (Skript.isAcceptRegistrations())) {
            SkriptAddon addon = Skript.registerAddon(getInstance());
            if (version.contains("1_15_R1")) {
                Bedrock.setInstance(this);
                try {
                    addon.loadClasses("com.moderocky.transk.syntax.blind", "effect");
                    addon.loadClasses("com.moderocky.transk.syntax.blind", "expression");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                getLogger().info("No BDI was found for this version.");
                getLogger().info("Complex translation syntax will be unavailable.");
            }
            try {
                addon.loadClasses("com.moderocky.transk.syntax.basic", "effect");
            } catch (IOException e) {
                e.printStackTrace();
            }
            getLogger().info("Everything appears to be in order.");
        } else {
            getLogger().info("Something has gone horribly wrong...");
            Bukkit.getPluginManager().disablePlugin(this);
        }
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public static BDI getBlindDataInterface() {
        return blindDataInterface;
    }

    public static String getMainPackage() {
        return TranSKator.class.getPackage().getName();
    }
}
