package com.moderocky.transk.api;


import java.util.Arrays;
import java.util.List;

public class Entry {

    protected String key;
    protected List<String> additives;

    protected Entry(String k, String... w) {
        key = k;
        additives = Arrays.asList(w);
    }

}
