package com.moderocky.transk.api.component;

import joptsimple.internal.Strings;

public class Axiomatic {

    private String concurrent;

    public Axiomatic() {
        concurrent = "";
    }

    public Axiomatic append(String s) {
        concurrent = concurrent + "," + s;
        return this;
    }

    public String get() {
        return concurrent;
    }



}
