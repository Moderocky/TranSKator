package com.moderocky.transk.api;

import com.moderocky.component.LoreComponent;
import com.moderocky.component.LoreTranslation;
import joptsimple.internal.Strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransAPI {

    public static LoreComponent convertKey(String string) {
        LoreComponent newLine;
        if (string.matches("(.+)\\[inputs='(.+)']")) {
            String key = string.split("\\[")[0];
            String values = string.split("\\[inputs=")[1];
            values = values.substring(1, values.length()-2);
            String[] inputs = values.split("','");
            if (inputs.length > 0) {
                newLine = new LoreTranslation(key, inputs);
            } else {
                newLine = new LoreTranslation(key);
            }
        } else {
            newLine = new LoreTranslation(string);
        }
        return newLine;
    }

    public static String convertStrings(String... strings) {
        if (strings.length > 1) {
            StringBuilder builder = new StringBuilder();
            builder.append(strings[0]);
            builder.append("[input='");
            List<String> inputs = new ArrayList<>(Arrays.asList(strings));
            if (inputs.size() > 0)
                inputs.remove(0);
            builder.append(Strings.join(inputs, "','"));
            builder.append("']");
            return builder.toString();
        } else if (strings.length == 1) {
            return strings[0];
        }
        else return null;
    }

}
