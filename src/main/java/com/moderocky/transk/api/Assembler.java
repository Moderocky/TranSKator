package com.moderocky.transk.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Assembler {

    private List<Entry> preliminaries;
    private List<String> auxiliaries;

    protected Assembler(Entry... entries) {
        preliminaries = Arrays.asList(entries);
    }

    protected void run() {
        auxiliaries = new ArrayList<>();
        preliminaries.forEach(entry -> {
            if (entry.additives.size() > 0)
                auxiliaries.add(new StringBuilder()
                        .append("'{\"translate\":\"lore.max_ammo\",")
                        .toString()
                );
            else ;

        });
    }

    protected List<String> collect() {
        return auxiliaries;
    }

}
