package com.moderocky.transk.api.blind;

import com.moderocky.transk.api.BDI;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.server.v1_15_R1.ItemStack;
import net.minecraft.server.v1_15_R1.MojangsonParser;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;

public class BDI_1_15_R1 implements BDI {

    @Override
    public boolean mergeCompound(org.bukkit.inventory.ItemStack i, Object c) {
        ItemStack s = (ItemStack) blind(i);
        NBTTagCompound n = new NBTTagCompound();
        if (s.getTag() != null)
            n = s.getTag();
        NBTTagCompound q = (NBTTagCompound) c;
        n.a(q);
        s.setTag(n);
        return true;
    }

    @Override
    public String getUnwitting(org.bukkit.inventory.ItemStack i) {
        return null;
    }

    @Override
    public Object getDisplayCompound(org.bukkit.inventory.ItemStack i) {
        return null;
    }

    @Override
    public org.bukkit.inventory.ItemStack build(String s) {
        return null;
    }

    @Override
    public Object blind(org.bukkit.inventory.ItemStack i) {
        return CraftItemStack.asNMSCopy(i);
    }

    @Override
    public Object parseCompound(String s) {
        NBTTagCompound nbt = new NBTTagCompound();
        try {
            return MojangsonParser.parse(s);
        } catch (CommandSyntaxException e) {
            return nbt;
        }
    }
}
