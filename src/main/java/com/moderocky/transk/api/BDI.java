package com.moderocky.transk.api;

import ch.njol.skript.aliases.ItemType;
import org.bukkit.inventory.ItemStack;

public interface BDI {

    boolean mergeCompound(ItemStack i, Object c);

    String getUnwitting(ItemStack i);

    Object getDisplayCompound(ItemStack i);

    ItemStack build(String s);

    Object blind(ItemStack i);

    Object parseCompound(String s);

    static ItemStack getItem(ItemType t) {
        return t.getRandom();
    }

    static void apply(ItemStack i, ItemType t) {
        t.setItemMeta(i.getItemMeta());
    }

}
