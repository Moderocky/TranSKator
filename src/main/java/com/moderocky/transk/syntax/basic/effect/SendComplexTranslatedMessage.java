package com.moderocky.transk.syntax.basic.effect;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TranslatableComponent;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import javax.annotation.Nullable;


@Name("Complex Translated Message")
@Description("Sends a complex translated message to players." +
        "\n  - This message can be a default one from the Minecraft lang file." +
        "\n  - It can also be a custom one included via mod or resource pack." +
        "\n  - Inside the language files, you can use § to colour messages." +
        "\n  - Complex messages can include placeholder values using '%s' in the lang file." +
        "\n  - These %s's are filled in with your inputs. To use normal percents, double the %%." +
        "\n  - You are also able to specify the order using '%1$s' and '%2$s'.")
@Examples({
        "send complex translated \"translation.test.args\" with inputs \"hello\" and \"there\" to player #Should send 'hello there'.",
        "send complex translated \"custom.message.key\" with inputs {_list-of-strings::*} to all players #Will send whatever this corresponds to in a custom language file."
})
@Since("0.0.1")
@SuppressWarnings("unused")
public class SendComplexTranslatedMessage extends Effect {

    static {
        Skript.registerEffect(SendComplexTranslatedMessage.class,
                "send [complex] translated [message[s]] %strings% with [(attachment[s]|input[s])] %strings% to %players%");
    }

    @SuppressWarnings("null")
    private Expression<String> stringExpression;
    private Expression<String> inputsExpression;
    private Expression<Player> playerExpression;

    @SuppressWarnings({"unchecked", "null"})
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        stringExpression = (Expression<String>) exprs[0];
        inputsExpression = (Expression<String>) exprs[1];
        playerExpression = (Expression<Player>) exprs[2];
        return true;
    }

    @Override
    protected void execute(Event event) {
        if (playerExpression == null || inputsExpression == null || stringExpression == null) return;

        for (String string : stringExpression.getArray(event)) {
            TranslatableComponent component = new TranslatableComponent(string);
            for (String input : inputsExpression.getArray(event))
                component.addWith(input);
            BaseComponent[] components = new ComponentBuilder(component).create();
            for (Player player : playerExpression.getArray(event)) {
                player.sendMessage(components);
            }
        }
    }

    @Override
    public String toString(@Nullable Event event, boolean debug) {
        String string = "<none>";
        String player = "<none>";
        String inputs = "<none>";
        if (stringExpression != null)
            string = stringExpression.toString(event, debug);
        if (inputsExpression != null)
            inputs = inputsExpression.toString(event, debug);
        if (playerExpression != null)
            player = playerExpression.toString(event, debug);
        return "send simple translated messages " + string + " with inputs " + inputs + " to " + player;
    }
}
