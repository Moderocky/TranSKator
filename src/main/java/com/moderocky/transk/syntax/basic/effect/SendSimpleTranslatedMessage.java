package com.moderocky.transk.syntax.basic.effect;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TranslatableComponent;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import javax.annotation.Nullable;


@Name("Simple Translated Message")
@Description("Sends a simple translated message to players." +
        "\n  - This message can be a default one from the Minecraft lang file." +
        "\n  - It can also be a custom one included via mod or resource pack." +
        "\n  - Inside the language files, you can use § to colour messages.")
@Examples({
        "send translated \"translation.test.none\" to player #Will send 'Hello, world!' in the player's locale.",
        "send translated \"custom.message.specified.in.lang_file\" to all players #Will send whatever this corresponds to in a custom language file."
})
@Since("0.0.1")
@SuppressWarnings("unused")
public class SendSimpleTranslatedMessage extends Effect {

    static {
        Skript.registerEffect(SendSimpleTranslatedMessage.class,
                "send [simple] translated [message[s]] %strings% to %players%");
    }

    @SuppressWarnings("null")
    private Expression<String> stringExpression;
    private Expression<Player> playerExpression;

    @SuppressWarnings({"unchecked", "null"})
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        stringExpression = (Expression<String>) exprs[0];
        playerExpression = (Expression<Player>) exprs[1];
        return true;
    }

    @Override
    protected void execute(Event event) {
        if (playerExpression == null || stringExpression == null) return;

        for (String string : stringExpression.getArray(event)) {
            BaseComponent[] components = new ComponentBuilder(new TranslatableComponent(string)).create();
            for (Player player : playerExpression.getArray(event)) {
                player.sendMessage(components);
            }
        }
    }

    @Override
    public String toString(@Nullable Event event, boolean debug) {
        String string = "<none>";
        String player = "<none>";
        if (stringExpression != null)
            string = stringExpression.toString(event, debug);
        if (playerExpression != null)
            player = playerExpression.toString(event, debug);
        return "send simple translated messages " + string + " to " + player;
    }
}
