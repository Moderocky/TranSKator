package com.moderocky.transk.syntax.blind.expression;

import ch.njol.skript.Skript;
import ch.njol.skript.aliases.ItemType;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import com.moderocky.component.LoreComponent;
import com.moderocky.component.LoreTranslation;
import com.moderocky.inventory.InvWrapper;
import com.moderocky.item.ItemStacker;
import com.moderocky.transk.api.TransAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;


@Name("Translated Item Lore")
@Description("Sets an item lore to translatable components." +
        "\n  - If you want to use a mixture of translated and normal text," +
        "\n    please use the 'complex' version of this syntax." +
        "\n  - You CANNOT have a translated component and normal text on the same line." +
        "\n  - This is due to a limitation in Minecraft's lore system." +
        "\n  - To get around this, consider registering a '%s%s' translation in your resource pack." +
        "\n  - Then you can embed multiple components into one another. :D")
@Examples({
        "set {_x} to player's tool with translated lore \"menu.singleplayer\" and \"custom.key\"",
        "set {_x} to player's tool with translated lore \"menu.singleplayer[inputs='§hi, I'm an input!','hello there!','General Kenobi!']\"",
        "set {_x} to player's tool with translated lore \"another.custom.key\" and \"§cI will look like this.\" #if there's no matching lang entry, the client will see your translation string."
})
@Since("0.1.2")
@SuppressWarnings("unused")
public class TranslatedLore extends SimpleExpression<ItemType> {

    static {
        Skript.registerExpression(TranslatedLore.class, ItemType.class, ExpressionType.SIMPLE,
                "%itemtype% with translated lore %strings%");
    }

    @SuppressWarnings("null")
    private Expression<ItemType> itemTypeExpression;
    private Expression<String> stringExpression;

    @SuppressWarnings({"unchecked", "null"})
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        itemTypeExpression = (Expression<ItemType>) exprs[0];
        stringExpression = (Expression<String>) exprs[1];
        return true;
    }

    @Override
    protected ItemType[] get(Event event) {
        List<ItemType> itemTypes = new ArrayList<>();
        ItemType itemType = itemTypeExpression.getSingle(event);
        ItemStack itemStack = itemType.getRandom();
        ItemStacker stacker = new ItemStacker(itemStack);
        List<LoreComponent> components = new ArrayList<>();
        for (String string : stringExpression.getArray(event)) {
            components.add(TransAPI.convertKey(string));
        }
        stacker.setComponentLore(components);
        itemType.setItemMeta(stacker.getItemMeta());
        itemTypes.add(itemType);
        return itemTypes.toArray(new ItemType[0]);
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends ItemType> getReturnType() {
        return ItemType.class;
    }

    @Override
    public String toString(@Nullable Event event, boolean debug) {
        String string = "<none>";
        String itemType = "<none>";
        String inputs = "<none>";
        if (stringExpression != null)
            string = stringExpression.toString(event, debug);
        if (itemTypeExpression != null)
            itemType = itemTypeExpression.toString(event, debug);
        return itemType + " with translated lore " + string;
    }
}
