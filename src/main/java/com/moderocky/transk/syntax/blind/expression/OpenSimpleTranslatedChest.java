package com.moderocky.transk.syntax.blind.expression;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import com.moderocky.inventory.InvWrapper;
import com.moderocky.transk.api.TransAPI;
import org.bukkit.event.Event;
import org.bukkit.inventory.Inventory;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;


@Name("Translated Chest Name")
@Description("Returns a chest with a translatable component name." +
        "\n  - This currently requires Minecraft version 1.15.1." +
        "\n  - Backwards compatibility may be added in future versions." +
        "\n  - Currently, the name may ONLY be your translated key.")
@Examples({
        "open a chest with translated name \"menu.singleplayer\" to player # Will be named 'Singleplayer' in whatever locale the player uses."
})
@Since("0.1.2")
@SuppressWarnings("unused")
public class OpenSimpleTranslatedChest extends SimpleExpression<Inventory> {

    static {
        Skript.registerExpression(OpenSimpleTranslatedChest.class, Inventory.class, ExpressionType.SIMPLE,
                "[a] [new] chest with [simple] translat(ed|able) name %string%");
    }

    @SuppressWarnings("null")
    private Expression<String> stringExpression;

    @SuppressWarnings({"unchecked", "null"})
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        stringExpression = (Expression<String>) exprs[0];
        return true;
    }

    @Override
    protected Inventory[] get(Event event) {
        List<Inventory> inventories = new ArrayList<>();
        inventories.add(InvWrapper.createChest(InvWrapper.translate(stringExpression.getSingle(event))));
        return inventories.toArray(new Inventory[0]);
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Inventory> getReturnType() {
        return Inventory.class;
    }

    @Override
    public String toString(@Nullable Event event, boolean debug) {
        String string = "<none>";
        String inputs = "<none>";
        if (stringExpression != null)
            string = stringExpression.toString(event, debug);
        return "a chest with translated name " + string;
    }
}
