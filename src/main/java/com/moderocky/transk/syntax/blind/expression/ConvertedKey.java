package com.moderocky.transk.syntax.blind.expression;

import ch.njol.skript.Skript;
import ch.njol.skript.aliases.ItemType;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import com.moderocky.component.LoreComponent;
import com.moderocky.component.LoreTranslation;
import com.moderocky.item.ItemStacker;
import com.moderocky.transk.api.TransAPI;
import joptsimple.internal.Strings;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Name("Converted Translation Key")
@Description("Creates a translation entry of the style key.here[inputs='input','input']." +
        "\n  - You provide it with a list of strings. Element 1 is the key, the rest will be added as inputs.")
@Examples({
        "set {_x} to player's tool with translated lore \"menu.singleplayer\" and \"custom.key\"",
        "set {_x} to player's tool with translated lore \"menu.singleplayer[inputs='§hi, I'm an input!','hello there!','General Kenobi!']\"",
        "set {_x} to player's tool with translated lore \"another.custom.key\" and \"§cI will look like this.\" #if there's no matching lang entry, the client will see your translation string."
})
@Since("0.1.2")
@SuppressWarnings("unused")
public class ConvertedKey extends SimpleExpression<String> {

    static {
        Skript.registerExpression(ConvertedKey.class, String.class, ExpressionType.SIMPLE,
                "[the] converted [translation] key %strings%");
    }

    @SuppressWarnings("null")
    private Expression<String> stringExpression;

    @SuppressWarnings({"unchecked", "null"})
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        stringExpression = (Expression<String>) exprs[0];
        return true;
    }

    @Override
    protected String[] get(Event event) {
        List<String> strings = new ArrayList<>();
        strings.add(TransAPI.convertStrings(stringExpression.getArray(event)));
        return strings.toArray(new String[0]);
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(@Nullable Event event, boolean debug) {
        String string = "<none>";
        if (stringExpression != null)
            string = stringExpression.toString(event, debug);
        return "converted translation key " + string;
    }
}
