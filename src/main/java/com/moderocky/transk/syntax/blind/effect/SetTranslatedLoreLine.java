package com.moderocky.transk.syntax.blind.effect;


import ch.njol.skript.Skript;
import ch.njol.skript.aliases.ItemType;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import com.moderocky.component.LoreComponent;
import com.moderocky.component.LoreLine;
import com.moderocky.component.LoreTranslation;
import com.moderocky.item.ItemStacker;
import com.moderocky.transk.api.TransAPI;
import com.moderocky.transk.syntax.basic.effect.SendSimpleTranslatedMessage;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TranslatableComponent;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

@Name("Set Translated Lore Line")
@Description("Sets a specific lore line of an item's lore to a translated value.")
@Examples({
        "set line 1 of {_item}'s lore to translated \"translation.test.none\""
})
@Since("0.1.2")
public class SetTranslatedLoreLine extends Effect {

    static {
        Skript.registerEffect(SetTranslatedLoreLine.class,
                "set line %number% of %itemtype%'[s] lore to translated %string%",
                "set line %number% of [the] lore of %itemtype% to translated %string%");
    }

    @SuppressWarnings("null")
    private Expression<Number> numberExpression;
    private Expression<String> stringExpression;
    private Expression<ItemType> itemTypeExpression;

    @SuppressWarnings({"unchecked", "null"})
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        numberExpression = (Expression<Number>) exprs[0];
        stringExpression = (Expression<String>) exprs[2];
        itemTypeExpression = (Expression<ItemType>) exprs[1];
        return true;
    }

    @Override
    protected void execute(Event event) {
        if (itemTypeExpression == null || stringExpression == null || numberExpression == null) return;

        ItemType itemType = itemTypeExpression.getSingle(event);
        ItemStack itemStack = itemType.getRandom();
        ItemStacker stacker = new ItemStacker(itemStack);
        int number = numberExpression.getSingle(event).intValue();
        List<LoreComponent> components = stacker.getComponentLore();
        LoreComponent newLine;
        String string = stringExpression.getSingle(event);
        newLine = TransAPI.convertKey(string);
        while (components.size() < number - 1) {
            components.add(new LoreLine());
        }
        components.set(number - 1 , newLine);

        stacker.setComponentLore(components);
        itemType.setItemMeta(stacker.getItemMeta());
    }

    @Override
    public String toString(@Nullable Event event, boolean debug) {
        String string = "<none>";
        String itemType = "<none>";
        String number = "<none>";
        if (stringExpression != null)
            string = stringExpression.toString(event, debug);
        if (itemTypeExpression != null)
            itemType = itemTypeExpression.toString(event, debug);
        if (numberExpression != null)
            number = numberExpression.toString(event, debug);
        return "set line " + number + " of " + itemType + "'s lore to translated " + string;
    }
}
