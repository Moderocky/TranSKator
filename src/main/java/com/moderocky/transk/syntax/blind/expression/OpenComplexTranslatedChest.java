package com.moderocky.transk.syntax.blind.expression;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import com.moderocky.inventory.InvWrapper;
import org.bukkit.event.Event;
import org.bukkit.inventory.Inventory;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;


@Name("Translated Chest Name")
@Description("Opens a chest with a translatable component name." +
        "\n  - This currently requires Minecraft version 1.15.1." +
        "\n  - Backwards compatibility may be added in future versions." +
        "\n  - Currently, the name may ONLY be your translated key.")
@Examples({
        "new chest with translated name \"translation.test.args\" with inputs \"hello\" and \"there\""
})
@Since("0.1.2")
@SuppressWarnings("unused")
public class OpenComplexTranslatedChest extends SimpleExpression<Inventory> {

    static {
        Skript.registerExpression(OpenComplexTranslatedChest.class, Inventory.class, ExpressionType.SIMPLE,
                "[a] [new] chest with [complex] translat(ed|able) name %string% with [(attachment[s]|input[s])] %strings%");
    }

    @SuppressWarnings("null")
    private Expression<String> stringExpression;
    private Expression<String> inputsExpression;

    @SuppressWarnings({"unchecked", "null"})
    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        stringExpression = (Expression<String>) exprs[0];
        inputsExpression = (Expression<String>) exprs[1];
        return true;
    }

    @Override
    protected Inventory[] get(Event event) {
        List<Inventory> inventories = new ArrayList<>();
        inventories.add(InvWrapper.createChest(InvWrapper.translate(stringExpression.getSingle(event), inputsExpression.getArray(event))));
        return inventories.toArray(new Inventory[0]);
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Inventory> getReturnType() {
        return Inventory.class;
    }

    @Override
    public String toString(@Nullable Event event, boolean debug) {
        String string = "<none>";
        String inputs = "<none>";
        if (stringExpression != null)
            string = stringExpression.toString(event, debug);
        if (inputsExpression != null)
            inputs = inputsExpression.toString(event, debug);
        return "open chest with translated name " + string + " with inputs " + inputs;
    }
}
